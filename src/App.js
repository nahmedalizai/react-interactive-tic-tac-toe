import React, { useState } from 'react';
import './App.css'
import 'hover.css'
import Table from './components/Table'
import Logo from './assets/logo.gif'
import WinX from './assets/winX.gif'
import WinO from './assets/winO.gif'
function App() {
  const [turn, setTurn] = useState(0)
  const [status, setStatus] = useState('P')
  const [isFinished, setIsFinished] = useState(false)
  const [resetBoard, setResetBoard] = useState(false)

  const setPlayerTurn = (current) => {
    current === 0 ? setTurn(1) : setTurn(0)
  }


  const resetGame = () => {
    console.log('reset game')
    setIsFinished(false)
    setTurn(0)
    setStatus('P')
    window.location.reload();
    return false;
  }

  function checkGameStatus(a) {
    let isEmptySquare = false
    let inp = ['X', 'O']
    let winner = null
    if (a.length === 9) {
      for (let i = 0; i < 9; i++) {
        if (a[i] === "") isEmptySquare = true
      }
      for (let i = 0; i < 2; i++) {
        if ((a[0] === inp[i] && a[1] === inp[i] && a[2] === inp[i]) || (a[0] === inp[i] && a[4] === inp[i] && a[8] === inp[i]) || (a[0] === inp[i] && a[3] === inp[i] && a[6] === inp[i])
          || (a[1] === inp[i] && a[4] === inp[i] && a[7] === inp[i]) || (a[2] === inp[i] && a[4] === inp[i] && a[6] === inp[i]) || (a[2] === inp[i] && a[5] === inp[i] && a[8] === inp[i])
          || (a[3] === inp[i] && a[4] === inp[i] && a[5] === inp[i]) || (a[6] === inp[i] && a[7] === inp[i] && a[8] === inp[i])) {
          winner = inp[i]
          setStatus(winner) // winner X | O
          setIsFinished(true)
          setResetBoard(true)
        }
      }
      if (winner === null && !isEmptySquare) {
        setStatus('D') // game is draw
        setIsFinished(true)
        setResetBoard(true)
      }      
      //setStatus('P') // game is pending
    }
  }
  return (
    <div className="App">
      <div className="App-Middle">
        <div>
          <table className='header'>
            <tbody>
              <tr>
                <td><div className="title">tic tac toe</div></td>
                <td><img src={Logo} alt="tic tac toe" /></td>
              </tr>
            </tbody>
          </table>
        </div>
        {resetBoard && status === 'X' && <div className='board'>
          <div>
            <p style={{color: '#282c34'}}>Player A is winner</p>
            <img src={WinX} alt="Winner player A" />
          </div>
        </div>}
        {resetBoard && status === 'O' && <div className='board'>
          <div>
            <p style={{color: '#282c34'}}>Player B is winner</p>
            <img src={WinO} alt="Winner player B" />
          </div>
        </div>}
        <div className='board'>
          <div style={{ margin: "10px" }}>
            {
              status === 'D' ? <div className='player-turn rose-red-color'>It's a draw!</div> :
                status === 'P' ?
                (turn === 0 ?
                  <div className='player-turn baby-blue-color'>Player A turn</div> :
                  <div className='player-turn parrot-green-color'>Player B turn</div>) : ''
            }
          </div>
          <Table playerTurn={setPlayerTurn} checkGameStatus={checkGameStatus} status={resetBoard}/>
        </div>
        {isFinished && <a href="#" className="btn btn-white btn-animate" onClick={resetGame}>Play again</a>}
      </div>
    </div>
  );
}

export default App;
