import React, { useState } from 'react'

function Table(props) {
    const [turn, setTurn] = useState(0)
    const [one, setOne] = useState('')
    const [two, setTwo] = useState('')
    const [three, setThree] = useState('')
    const [four, setFour] = useState('')
    const [five, setFive] = useState('')
    const [six, setSix] = useState('')
    const [seven, setSeven] = useState('')
    const [eight, setEight] = useState('')
    const [nine, setNine] = useState('')
    let squares = new Array(9).fill('')

    const setPlayerTurn = (event) => {
        if (event.currentTarget.innerText === "") {
            if (event.currentTarget.id === "0") {
                setOne(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O';
            }
            if (event.currentTarget.id === "1") {
                setTwo(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O'
            }
            if (event.currentTarget.id === "2") {
                setThree(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O'
            }
            if (event.currentTarget.id === "3") {
                setFour(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O'
            }
            if (event.currentTarget.id === "4") {
                setFive(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O'
            }
            if (event.currentTarget.id === "5") {
                setSix(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O'
            }
            if (event.currentTarget.id === "6") {
                setSeven(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O'
            }
            if (event.currentTarget.id === "7") {
                setEight(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O'
            }
            if (event.currentTarget.id === "8") {
                setNine(turn === 0 ? 'X' : 'O')
                squares[parseInt(event.currentTarget.id)] = turn === 0 ? 'X' : 'O'
            }

            for (let i = 0; i < squares.length; i++) {
                if (parseInt(event.currentTarget.id) !== i)
                    squares[i] = document.getElementById(i.toString()).innerText;
            }
            turn === 0 ? setTurn(1) : setTurn(0)
            props.playerTurn(turn)
            props.checkGameStatus(squares)
        }
    }

    return (
        <div disabled={props.status}>
            <table>
                <tbody>
                    <tr>
                        <td id='0' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {one}
                            </div>
                        </td>
                        <td id='1' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {two}
                            </div>
                        </td>
                        <td id='2' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {three}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td id='3' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {four}
                            </div>
                        </td>
                        <td id='4' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {five}
                            </div>
                        </td>
                        <td id='5' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {six}
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td id='6' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {seven}
                            </div>
                        </td>
                        <td id='7' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {eight}
                            </div>
                        </td>
                        <td id='8' className='card' onClick={event => setPlayerTurn(event)}>
                            <div className='card_title title-white'>
                                {nine}
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default Table
